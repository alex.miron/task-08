package tools;

import com.epam.rd.java.basic.task8.Entity.Flower;
import com.epam.rd.java.basic.task8.Entity.Flowers;
import com.epam.rd.java.basic.task8.constants.FlowersConstant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import parser.GrowingTipsDOM;
import parser.VisualParametersDOM;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

public class Preservation {
    public static void save(Flowers flowers, String xmlFileName){
        try {
            FileOutputStream file = new FileOutputStream(xmlFileName);
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element el = parseFlowersToXML(document,flowers);
            document.appendChild(el);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult streamResult = new StreamResult(file);
            transformer.transform(source,streamResult);
        } catch (FileNotFoundException | ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private static Element parseFlowersToXML(Document document, Flowers flowers){
        Element el = document.createElement(FlowersConstant.NODE_NAME1.getFlowers());
        for (Map.Entry<String,String> entry: flowers.getAttribute().entrySet()) {
            el.setAttribute(entry.getKey(), entry.getValue());
        }
        for(Flower flower:flowers.getFlowers()){
            el.appendChild(parseFlowerToXML(document,flower));
        }
        return el;
    }
    private static  Element parseFlowerToXML(Document document, Flower flower){
        Element flowerEl = document.createElement(FlowersConstant.NODE_NAME2.getFlowers());
        Element name = document.createElement(FlowersConstant.ELEMENT_NAME.getFlowers());
        Element soil = document.createElement(FlowersConstant.ELEMENT_SOIL.getFlowers());
        Element origin = document.createElement(FlowersConstant.ELEMENT_ORIGIN.getFlowers());
        Element multiplying = document.createElement(FlowersConstant.ELEMENT_MULTIPLYING.getFlowers());
        Element visualParameters = VisualParametersDOM.parseToXML(document,flower.getVisualParameters());
        Element growingTips = GrowingTipsDOM.parseToXML(document,flower.getGrowingTips());
        name.setTextContent(flower.getName());
        soil.setTextContent(flower.getSoil());
        origin.setTextContent(flower.getOrigin());
        multiplying.setTextContent(flower.getMultiplying());

        flowerEl.appendChild(name);
        flowerEl.appendChild(soil);
        flowerEl.appendChild(origin);
        flowerEl.appendChild(visualParameters);
        flowerEl.appendChild(growingTips);
        flowerEl.appendChild(multiplying);
        return flowerEl;
    }
}
