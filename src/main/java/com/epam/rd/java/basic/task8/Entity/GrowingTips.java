package com.epam.rd.java.basic.task8.Entity;

import java.math.BigInteger;

public class GrowingTips {
    protected Temperature temperature;
    protected Watering watering;
    protected String lighting;
    public GrowingTips(){

    }

    public GrowingTips(Temperature temperature, Watering watering, String lighting) {
        this.temperature = temperature;
        this.watering = watering;
        this.lighting = lighting;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", watering=" + watering +
                ", lighting=" + lighting +
                '}';
    }

    public static class Temperature{

        protected BigInteger value;
        protected String measure;

        public Temperature(){

        }
        public Temperature(BigInteger value,String measure){
            this.measure = measure;
            this.value = value;
        }
        public BigInteger getValue() {
            return value;
        }

        public void setValue(BigInteger value) {
            this.value = value;
        }

        public String getMeasure() {
            if (measure == null) {
                return "celcius";
            } else {
                return measure;
            }
        }
        public void setMeasure(String value) {
            this.measure = value;
        }
        @Override
        public String toString() {
            return "Temperature{" +
                    "value=" + value +
                    ", measure='" + measure + '\'' +
                    '}';
        }
    }
    public static class Watering{
        protected BigInteger value;
        protected String measure;

        public Watering(){

        }
        public Watering(BigInteger value, String measure) {
            this.value = value;
            this.measure = measure;
        }

        public BigInteger getValue() {
            return value;
        }

        public void setValue(BigInteger value) {
            this.value = value;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "Watering{" +
                    "value=" + value +
                    ", measure='" + measure + '\'' +
                    '}';
        }
    }
}
