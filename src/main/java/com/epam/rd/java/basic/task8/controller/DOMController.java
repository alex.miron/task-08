package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Entity.Flower;
import com.epam.rd.java.basic.task8.Entity.Flowers;
import com.epam.rd.java.basic.task8.Entity.GrowingTips;
import com.epam.rd.java.basic.task8.Entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.FlowersConstant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import parser.GrowingTipsDOM;
import parser.VisualParametersDOM;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	public Flowers parse(){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Flowers flowers = null;
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File(xmlFileName));
			document.getDocumentElement().normalize();
			flowers = parserGetXML(document.getDocumentElement());
		} catch (ParserConfigurationException | IOException|SAXException e) {
			e.printStackTrace();
		}
		return flowers;
	}
	private Flowers parserGetXML(Element el){
		Flowers flowers = new Flowers();
		NodeList nodeList = el.getElementsByTagName(FlowersConstant.NODE_NAME2.getFlowers());
		for (int i = 0; i <el.getAttributes().getLength() ; i++) {
			Node node = el.getAttributes().item(i);
			flowers.addAttribute(node.getNodeName(), node.getTextContent());
		}
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element element = (Element) nodeList.item(i);
			Flower flower = parserFlowerGetXML(element);
			flowers.addFlower(flower);
		}
		return flowers;
	}
	private Flower parserFlowerGetXML(Element el){
		String name = el.getElementsByTagName(FlowersConstant.ELEMENT_NAME.getFlowers()).item(0).getTextContent();
		String soil = el.getElementsByTagName(FlowersConstant.ELEMENT_SOIL.getFlowers()).item(0).getTextContent();
		String origin = el.getElementsByTagName(FlowersConstant.ELEMENT_ORIGIN.getFlowers()).item(0).getTextContent();
		String multiplying = el.getElementsByTagName(FlowersConstant.ELEMENT_MULTIPLYING.getFlowers()).item(0)
				.getTextContent();
		VisualParameters visualParameters = VisualParametersDOM.parseGetXML(el);
		GrowingTips growingTips = GrowingTipsDOM.parseGetXML(el);
		return new Flower(name,soil,origin,visualParameters,growingTips,multiplying);
	}

	// PLACE YOUR CODE HERE

}
