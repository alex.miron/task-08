package com.epam.rd.java.basic.task8.Entity;

import java.util.*;

public class Flowers {
    protected List<Flower> flowers;
    protected Map<String, String> attribute;

    public Flowers(){
        this.flowers = new ArrayList<>();
        this.attribute = new LinkedHashMap<>();
    }

    public Map<String, String> getAttribute() {
        return this.attribute;
    }

    public void setAttribute(Map<String, String> attribute) {
        this.attribute = attribute;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }
    public void sortByName(){
        flowers.sort(Comparator.comparing(Flower::getName));
    }
    public void sortByOrigin(){
        flowers.sort(Comparator.comparing(Flower::getOrigin));
    }
    public void sortByAveLenFlower(){
        flowers.sort(Comparator.comparing(s->s.getVisualParameters().getAveLenFlower().getValue()));
    }
    public void addAttribute(String a , String b){
        attribute.put(a, b);
    }
    public void addFlower(Flower flower){
        flowers.add(flower);
    }
}
