package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Entity.Flower;
import com.epam.rd.java.basic.task8.Entity.Flowers;
import com.epam.rd.java.basic.task8.Entity.GrowingTips;
import com.epam.rd.java.basic.task8.Entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.FlowersConstant;
import com.epam.rd.java.basic.task8.constants.GrowingTipsConstant;
import com.epam.rd.java.basic.task8.constants.VisualParametersConstant;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private Flower flower;
	private Flowers flowers;
	private StringBuilder currentValue = new StringBuilder();
;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	public Flowers parse(){
		flowers  = new Flowers();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(xmlFileName,this);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowers;
	}
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentValue.setLength(0);
		if(qName.equalsIgnoreCase(FlowersConstant.NODE_NAME1.getFlowers())){
			for (int i = 0; i <attributes.getLength() ; i++) {
				flowers.addAttribute(attributes.getQName(i),attributes.getValue(i));
			}
		}
		if(qName.equalsIgnoreCase(FlowersConstant.NODE_NAME2.getFlowers())){
			flower = new Flower();
		}else if(qName.equalsIgnoreCase(VisualParametersConstant.NODE_NAME.getVisualParameters())){
			flower.setVisualParameters(new VisualParameters());
		}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_AVE_LEN_FLOWER.getVisualParameters())){
			VisualParameters.AveLenFlower aveLenFlower = new VisualParameters.AveLenFlower();
			aveLenFlower.setMeasure(attributes.getValue(VisualParametersConstant.ELEMENT_MEASURE
					.getVisualParameters()));
			flower.getVisualParameters().setAveLenFlower(aveLenFlower);
		}else if(qName.equalsIgnoreCase(GrowingTipsConstant.NODE_NAME.getGrowingTips())){
			flower.setGrowingTips(new GrowingTips());
		}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_TEMPERATURE.getGrowingTips())){
			GrowingTips.Temperature temperature = new GrowingTips.Temperature();
			temperature.setMeasure(attributes.getValue(GrowingTipsConstant.ELEMENT_TEMPERATURE_MEASURE
					.getGrowingTips()));
			flower.getGrowingTips().setTemperature(temperature);
		}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_LIGHTING.getGrowingTips())){
			flower.getGrowingTips().setLighting(attributes.getValue(GrowingTipsConstant.ELEMENT_LIGHT_REQUIRING.getGrowingTips()));
		}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_WATERING.getGrowingTips())){
			GrowingTips.Watering watering = new GrowingTips.Watering();
			watering.setMeasure(attributes.getValue(GrowingTipsConstant.ELEMENT_WATERING_MEASURE.getGrowingTips()));
			flower.getGrowingTips().setWatering(watering);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName.equalsIgnoreCase(FlowersConstant.NODE_NAME2.getFlowers())){
			flowers.addFlower(flower);
		}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_NAME.getFlowers())){
			flower.setName(currentValue.toString());
		}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_SOIL.getFlowers())){
			flower.setSoil(currentValue.toString());
		}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_ORIGIN.getFlowers())){
			flower.setOrigin(currentValue.toString());
		}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_STEM_COLOUR.getVisualParameters())){
			flower.getVisualParameters().setStemColour(currentValue.toString());
		}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_LEAF_COLOR.getVisualParameters())){
			flower.getVisualParameters().setLeafColour(currentValue.toString());
		}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_AVE_LEN_FLOWER.getVisualParameters())){
			flower.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(currentValue.toString()));
		}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_TEMPERATURE.getGrowingTips())){
			flower.getGrowingTips().getTemperature().setValue(BigInteger
					.valueOf(Long.parseLong(currentValue.toString())));
		}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_WATERING.getGrowingTips())){
			flower.getGrowingTips().getWatering().setValue(BigInteger.valueOf(Long.parseLong(currentValue.toString())));
		}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_MULTIPLYING.getFlowers())){
			flower.setMultiplying(currentValue.toString());
		}
	}


	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		currentValue.append(ch,start,length);
	}
	// PLACE YOUR CODE HERE
}