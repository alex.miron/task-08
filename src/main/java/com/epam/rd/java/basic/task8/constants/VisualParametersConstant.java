package com.epam.rd.java.basic.task8.constants;

public enum VisualParametersConstant {
    NODE_NAME("visualParameters"),
    ELEMENT_STEM_COLOUR("stemColour"),
    ELEMENT_LEAF_COLOR("leafColour"),
    ELEMENT_AVE_LEN_FLOWER("aveLenFlower"),
    ELEMENT_MEASURE("measure");
    private final String visualParameters;
    VisualParametersConstant(String visualParameters) {
        this.visualParameters = visualParameters;
    }
    public String getVisualParameters() {
        return visualParameters;
    }
}
