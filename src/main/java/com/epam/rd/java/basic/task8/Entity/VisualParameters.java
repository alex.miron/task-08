package com.epam.rd.java.basic.task8.Entity;

import java.math.BigInteger;

public class VisualParameters {
    protected String stemColour;
    protected String leafColour;
    protected AveLenFlower aveLenFlower;
    public VisualParameters(){

    }

    public VisualParameters(String stemColour, String leafColour, AveLenFlower aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }

    public static class AveLenFlower{
        protected int value;
        protected String measure;
        public AveLenFlower(){

        }

        public AveLenFlower(int value, String measure) {
            this.value = value;
            this.measure = measure;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "AveLenFlower{" +
                    "value=" + value +
                    ", measure='" + measure + '\'' +
                    '}';
        }
    }

}
