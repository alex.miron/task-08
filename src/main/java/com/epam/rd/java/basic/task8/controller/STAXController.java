package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Entity.Flower;
import com.epam.rd.java.basic.task8.Entity.Flowers;
import com.epam.rd.java.basic.task8.Entity.GrowingTips;
import com.epam.rd.java.basic.task8.Entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.FlowersConstant;
import com.epam.rd.java.basic.task8.constants.GrowingTipsConstant;
import com.epam.rd.java.basic.task8.constants.VisualParametersConstant;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flowers flowers;
	private Flower flower;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public Flowers parse(){
		flowers = new Flowers();
		try {
			XMLEventReader reader = XMLInputFactory.newInstance()
					.createXMLEventReader(new FileInputStream(xmlFileName));
			while(reader.hasNext()){
				XMLEvent xmlEvent = reader.nextEvent();
				if(xmlEvent.isStartElement()){
					StartElement startElement = xmlEvent.asStartElement();
					String qName = startElement.getName().getLocalPart();
					if (qName.equalsIgnoreCase(FlowersConstant.NODE_NAME1.getFlowers())){
						startElement.getNamespaces().forEachRemaining(
								namespace ->{
									String key = Stream.of(namespace.getName().getPrefix(),
											namespace.getName().getLocalPart()).filter(s->!s.equals(""))
											.collect(Collectors.joining(":"));
									flowers.addAttribute(key, namespace.getValue());
								} );
						startElement.getAttributes().forEachRemaining(
								attribute -> {
									String key = Stream.of(attribute.getName().getPrefix(),
											attribute.getName().getLocalPart()).filter(s->!s.equals(""))
											.collect(Collectors.joining(":"));
									flowers.addAttribute(key, attribute.getValue());
								}
						);
					}else if(qName.equalsIgnoreCase(FlowersConstant.NODE_NAME2.getFlowers())){
						flower = new Flower();
					}else if(qName.equals(FlowersConstant.ELEMENT_NAME.getFlowers())){
						xmlEvent = reader.nextEvent();
						flower.setName(xmlEvent.asCharacters().getData());
					}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_SOIL.getFlowers())){
						xmlEvent = reader.nextEvent();
						flower.setSoil(xmlEvent.asCharacters().getData());
					}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_ORIGIN.getFlowers())){
						xmlEvent = reader.nextEvent();
						flower.setOrigin(xmlEvent.asCharacters().getData());
					}else if(qName.equalsIgnoreCase(VisualParametersConstant.NODE_NAME.getVisualParameters())){
						flower.setVisualParameters(new VisualParameters());
					}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_STEM_COLOUR.getVisualParameters())){
						xmlEvent = reader.nextEvent();
						flower.getVisualParameters().setStemColour(xmlEvent.asCharacters().getData());
					}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_LEAF_COLOR.getVisualParameters())){
						xmlEvent = reader.nextEvent();
						flower.getVisualParameters().setLeafColour(xmlEvent.asCharacters().getData());
					}else if(qName.equalsIgnoreCase(VisualParametersConstant.ELEMENT_AVE_LEN_FLOWER.getVisualParameters())){
						VisualParameters.AveLenFlower aveLenFlower = new VisualParameters.AveLenFlower();
						aveLenFlower.setMeasure(startElement.getAttributeByName(new QName(VisualParametersConstant
								.ELEMENT_MEASURE.getVisualParameters())).getValue());
						xmlEvent = reader.nextEvent();
						aveLenFlower.setValue(Integer.parseInt(xmlEvent.asCharacters().getData()));
						flower.getVisualParameters().setAveLenFlower(aveLenFlower);
					}else if(qName.equalsIgnoreCase(GrowingTipsConstant.NODE_NAME.getGrowingTips())){
						flower.setGrowingTips(new GrowingTips());
					}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_TEMPERATURE.getGrowingTips())){
						GrowingTips.Temperature temperature = new GrowingTips.Temperature();
						temperature.setMeasure(startElement.getAttributeByName(new QName(GrowingTipsConstant
								.ELEMENT_TEMPERATURE_MEASURE.getGrowingTips())).getValue());
						xmlEvent = reader.nextEvent();
						temperature.setValue(BigInteger.valueOf(Long.parseLong(xmlEvent.asCharacters().getData())));
						flower.getGrowingTips().setTemperature(temperature);
					}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_LIGHTING.getGrowingTips())){
						flower.getGrowingTips().setLighting(startElement.getAttributeByName(new QName(
								GrowingTipsConstant.ELEMENT_LIGHT_REQUIRING.getGrowingTips()
						)).getValue());
					}else if(qName.equalsIgnoreCase(GrowingTipsConstant.ELEMENT_WATERING.getGrowingTips())){
						GrowingTips.Watering watering = new GrowingTips.Watering();
						watering.setMeasure(startElement.getAttributeByName(new QName(GrowingTipsConstant
								.ELEMENT_WATERING_MEASURE.getGrowingTips())).getValue());
						xmlEvent = reader.nextEvent();
						watering.setValue(BigInteger.valueOf(Long.parseLong(xmlEvent.asCharacters().getData())));
						flower.getGrowingTips().setWatering(watering);
					}else if(qName.equalsIgnoreCase(FlowersConstant.ELEMENT_MULTIPLYING.getFlowers())){
						xmlEvent =  reader.nextEvent();
						flower.setMultiplying(xmlEvent.asCharacters().getData());
					}
				}
				if(xmlEvent.isEndElement()){
					EndElement element = xmlEvent.asEndElement();
					if(element.getName().getLocalPart().equalsIgnoreCase(FlowersConstant.NODE_NAME2.getFlowers())){
						flowers.addFlower(flower);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return flowers;
	}
}