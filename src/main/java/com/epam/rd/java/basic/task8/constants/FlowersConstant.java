package com.epam.rd.java.basic.task8.constants;

public enum FlowersConstant {
    NODE_NAME1("flowers"),
    NODE_NAME2("flower"),
    ELEMENT_NAME("name"),
    ELEMENT_SOIL("soil"),
    ELEMENT_ORIGIN("origin"),
    ELEMENT_MULTIPLYING("multiplying");
    private final String flowers;
    FlowersConstant(String flowers) {
        this.flowers = flowers;
    }
    public String getFlowers() {
        return flowers;
    }
}