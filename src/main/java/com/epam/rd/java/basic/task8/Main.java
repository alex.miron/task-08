package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.Entity.Flowers;
import com.epam.rd.java.basic.task8.controller.*;
import tools.Preservation;
import tools.Valid;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersDOM = domController.parse();
//		System.out.println(flowersDOM.getFlowers());

		// sort (case 1)
		flowersDOM.sortByName();
		// PLACE YOUR CODE HERE
		
		// save

		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		Preservation.save(flowersDOM, outputXmlFile);
		boolean isValidDOM = Valid.validateXML(outputXmlFile, "input.xsd");
		System.out.println("isValidDOM:" + isValidDOM);
//		System.out.println(flowersDOM.getFlowers());
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSAX = saxController.parse();
//		System.out.println(flowersSAX.getFlowers());
		
		// sort  (case 2)
		flowersSAX.sortByOrigin();
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile1 = "output.sax.xml";
		// PLACE YOUR CODE HERE
		Preservation.save(flowersSAX, outputXmlFile1);
		boolean isValidSAX = Valid.validateXML(outputXmlFile1,"input.xsd");
		System.out.println("isValidSAX:" + isValidSAX);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSTAX = staxController.parse();
		
		// sort  (case 3)
		flowersSTAX.sortByAveLenFlower();
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile3 = "output.stax.xml";
		Preservation.save(flowersSTAX,outputXmlFile3);
		boolean isValidSTAX = Valid.validateXML(outputXmlFile3, "input.xsd");
		System.out.println("isValidSTAX:" + isValidSTAX);
		// PLACE YOUR CODE HERE
	}

}
