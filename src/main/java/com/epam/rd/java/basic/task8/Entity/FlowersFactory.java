package com.epam.rd.java.basic.task8.Entity;

public class FlowersFactory {
    public FlowersFactory(){

    }
    public Flowers createFlowers(){return new Flowers();}

    public Flower createFlower(){return new Flower();}

    public GrowingTips createGrowingTips(){return new GrowingTips();}

    public VisualParameters createVisualParameters(){return new VisualParameters();}

    public GrowingTips.Temperature createTemperature(){return new GrowingTips.Temperature();}

    public GrowingTips.Watering createWatering(){return new GrowingTips.Watering();}

    public VisualParameters.AveLenFlower createAveLenFlower(){return new VisualParameters.AveLenFlower();}
}
