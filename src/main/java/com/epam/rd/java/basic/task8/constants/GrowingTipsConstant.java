package com.epam.rd.java.basic.task8.constants;

public enum GrowingTipsConstant {
    NODE_NAME("growingTips"),
    ELEMENT_LIGHTING("lighting"),
    ELEMENT_LIGHT_REQUIRING("lightRequiring"),
    ELEMENT_TEMPERATURE("tempreture"),
    ELEMENT_TEMPERATURE_MEASURE("measure"),
    ELEMENT_WATERING("watering"),
    ELEMENT_WATERING_MEASURE("measure");
    private final String growingTips;
    GrowingTipsConstant(String growingTips) {
        this.growingTips = growingTips;
    }

    public String getGrowingTips() {
        return growingTips;
    }
}
