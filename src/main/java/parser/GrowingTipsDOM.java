package parser;

import com.epam.rd.java.basic.task8.Entity.GrowingTips;
import com.epam.rd.java.basic.task8.constants.GrowingTipsConstant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.math.BigInteger;

public class GrowingTipsDOM {
    public static GrowingTips parseGetXML(Element el){
        Node node = el.getElementsByTagName(GrowingTipsConstant.NODE_NAME.getGrowingTips()).item(0);
        Element elementNode = (Element) node;
        String lighting =parseLightingGetXML(elementNode);
        GrowingTips.Temperature temperature = parseTemperatureGetXML(elementNode);
        GrowingTips.Watering watering = parseWateringGetXML(elementNode);
        return new GrowingTips(temperature,watering,lighting);
    }
    public static Element parseToXML(Document document, GrowingTips growingTips){
        Element growingTipsEl = document.createElement(GrowingTipsConstant.NODE_NAME.getGrowingTips());
        Element lighting = parseLightingToXML(document, growingTips.getLighting());
        Element temperature = parseTemperatureToXML(document, growingTips.getTemperature());
        Element watering = parseWateringToXML(document, growingTips.getWatering());
        growingTipsEl.appendChild(temperature);
        growingTipsEl.appendChild(lighting);
        growingTipsEl.appendChild(watering);
        return growingTipsEl;
    }
    private static String parseLightingGetXML(Element el){
        Node node = el.getElementsByTagName(GrowingTipsConstant.ELEMENT_LIGHTING.getGrowingTips()).item(0);
        return node.getAttributes().getNamedItem(GrowingTipsConstant.ELEMENT_LIGHT_REQUIRING.getGrowingTips())
                .getTextContent();
    }
    private static Element parseLightingToXML(Document document, String lighting){
        Element lightingEl = document.createElement(GrowingTipsConstant.ELEMENT_LIGHTING.getGrowingTips());
        lightingEl.setAttribute(GrowingTipsConstant.ELEMENT_LIGHT_REQUIRING.getGrowingTips(), lighting);
        return lightingEl;
    }
    private static GrowingTips.Temperature parseTemperatureGetXML(Element el){
        Node node = el.getElementsByTagName(GrowingTipsConstant.ELEMENT_TEMPERATURE.getGrowingTips()).item(0);
        String measure = node.getAttributes().getNamedItem(GrowingTipsConstant.ELEMENT_TEMPERATURE_MEASURE.getGrowingTips())
                .getTextContent();
        BigInteger value = BigInteger.valueOf(Long.parseLong(node.getTextContent()));
        return new GrowingTips.Temperature(value, measure);
    }
    private static  Element parseTemperatureToXML(Document document, GrowingTips.Temperature temperature){
        Element temperatureEl = document.createElement(GrowingTipsConstant.ELEMENT_TEMPERATURE.getGrowingTips());
        temperatureEl.setAttribute(GrowingTipsConstant.ELEMENT_TEMPERATURE_MEASURE.getGrowingTips(),
                temperature.getMeasure());
        temperatureEl.setTextContent(String.valueOf(temperature.getValue()));
        return temperatureEl;
    }
    private static GrowingTips.Watering parseWateringGetXML(Element el){
        Node node = el.getElementsByTagName(GrowingTipsConstant.ELEMENT_WATERING.getGrowingTips()).item(0);
        String measure = node.getAttributes().getNamedItem(GrowingTipsConstant.ELEMENT_WATERING_MEASURE.getGrowingTips())
                .getTextContent();
        BigInteger value = BigInteger.valueOf(Long.parseLong(node.getTextContent()));
        return new GrowingTips.Watering(value,measure);
    }
    private static Element parseWateringToXML(Document document, GrowingTips.Watering watering){
        Element wateringEl = document.createElement(GrowingTipsConstant.ELEMENT_WATERING.getGrowingTips());
        wateringEl.setAttribute(GrowingTipsConstant.ELEMENT_WATERING_MEASURE.getGrowingTips(), watering.getMeasure());
        wateringEl.setTextContent(String.valueOf(watering.getValue()));
        return wateringEl;
    }
}
