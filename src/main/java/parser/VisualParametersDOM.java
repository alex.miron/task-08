package parser;

import com.epam.rd.java.basic.task8.Entity.VisualParameters;
import com.epam.rd.java.basic.task8.constants.VisualParametersConstant;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class VisualParametersDOM {
    public static VisualParameters parseGetXML(Element element){
        Node node = element.getElementsByTagName(VisualParametersConstant.NODE_NAME.getVisualParameters()).item(0);
        Element elementNode = (Element) node;
        String stemColour = elementNode.getElementsByTagName(VisualParametersConstant.ELEMENT_STEM_COLOUR.getVisualParameters())
                .item(0).getNodeName();
        String leafColour = elementNode.getElementsByTagName(VisualParametersConstant.ELEMENT_LEAF_COLOR.getVisualParameters())
                .item(0).getNodeName();
        VisualParameters.AveLenFlower aveLenFlower = parseGetXMLAveLenFlower(elementNode);

        return new VisualParameters(stemColour,leafColour,aveLenFlower);
    }
    private static VisualParameters.AveLenFlower parseGetXMLAveLenFlower(Element element){
        Node node = element.getElementsByTagName(VisualParametersConstant.ELEMENT_AVE_LEN_FLOWER.getVisualParameters())
                .item(0);
        String measure = node.getAttributes().getNamedItem(VisualParametersConstant.ELEMENT_MEASURE.getVisualParameters())
                .getTextContent();
        int value = Integer.parseInt(node.getTextContent());
        return new VisualParameters.AveLenFlower(value,measure);
    }
    public static  Element parseToXML(Document document, VisualParameters visualParameters){
        Element visualParametersEl = document.createElement(VisualParametersConstant.NODE_NAME.getVisualParameters());
        Element stemColour = document.createElement(VisualParametersConstant.ELEMENT_STEM_COLOUR.getVisualParameters());
        Element leafColour = document.createElement(VisualParametersConstant.ELEMENT_LEAF_COLOR.getVisualParameters());
        Element aveLenFlower = parseAveLenFlowerToXML(document,visualParameters.getAveLenFlower());
        stemColour.setTextContent(visualParameters.getStemColour());
        leafColour.setTextContent(visualParameters.getLeafColour());
        visualParametersEl.appendChild(stemColour);
        visualParametersEl.appendChild(leafColour);
        visualParametersEl.appendChild(aveLenFlower);
        return visualParametersEl;
    }
    private static Element parseAveLenFlowerToXML(Document document, VisualParameters.AveLenFlower aveLenFlower){
        Element aveLenFlowerEl = document.createElement(VisualParametersConstant.ELEMENT_AVE_LEN_FLOWER
                .getVisualParameters());
        aveLenFlowerEl.setAttribute(VisualParametersConstant.ELEMENT_MEASURE.getVisualParameters()
                , aveLenFlower.getMeasure());
        aveLenFlowerEl.setTextContent(String.valueOf(aveLenFlower.getValue()));
        return aveLenFlowerEl;
    }
}
